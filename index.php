<?php
require ("class_postings.php"); $Postings = new Postings;
require ("class_arraytoxml.php"); $ATX = new ArrayToXML;
include_once('tinybutstrong.php'); $TBS = new clsTinyButStrong;
require_once "spyc.php";
function xml2array ( $xmlObject, $out = array () )
{
    foreach ( (array) $xmlObject as $index => $node )
        $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

    return $out;
}

/*  Am I logged in?
_______________________*/
//check whether logged in
$allusers = include("users.php");
// echo $staceyversion;
//$allusers = null;
// print_r($allusers);
// exit();
if ( 0 == strlen( $allusers ) && empty(get_included_files()) ){ echo "Casey isn't installed. <a href=\"install.php\">You can do that now.</a>"; exit(); } 
if ($allusers == 1 && __FILE__ == get_included_files()[0]){ echo "Casey isn't installed. <a href=\"install.php\">You can do that now.</a>"; exit(); } 
if (strlen($allusers) > 24) { $xml = new SimpleXMLElement($allusers); }
// print_r($xml);
if (isset($_COOKIE['pass'])) { $nodes = $xml->xpath(sprintf(" //users/user[(password = \"$_COOKIE[pass]\") and (publickey = \"$_COOKIE[publickey]\")] ")); }
if (!empty($nodes) && !isset($_POST['changepass']) ) {
            $userxmlRaw = $ATX->toXml($nodes[0]); $userxml = new SimpleXMLElement($userxmlRaw);
            if ( $_COOKIE['pass'] == $userxml->password && $_COOKIE['publickey'] == $userxml->publickey) { $logged = "yes";} 
            else { $logged = null; }
} elseif(isset($_POST['oldpassword'])){
    // exit("xx");
        $userxmlRaw = $ATX->toXml($nodes[0]); $userxml = new SimpleXMLElement($userxmlRaw);
        $datask = $Postings->DoIKnowMyPassword($_POST['myusername'], $_POST['oldpassword']);
        // var_dump($datask)
        if ($datask === false){ $notice = "You failed to provide your correct current password. Try again."; $logged = "yes"; }
} elseif(isset($_POST['password1']) && $_POST['password1'] != $_POST['password2']){
    $userxmlRaw = $ATX->toXml($nodes[0]); $userxml = new SimpleXMLElement($userxmlRaw);
    $logged = "yes";
    $notice = "Both your new passwords do not match. Try again.";
    //exit("testing 123 hello");
    // i'm trying to change my password but both don't match
    //do nothing
    // don't change the $logged variable
    // for the sake of cleaner code, I'm not combining the if() and elseif() into one if() but with an OR || operator
} else { 
    $logged = null;
}

/*  Posting classes
_______________________*/

if (isset($_POST['changepass']) && isset($logged) && $logged == "yes" && $userxml->username != 'demo' && (isset($_POST['password1'])) && $_POST['password1'] == $_POST['password2']){
    //$datask = $Postings->ChangePassword("$userxml->username", "$_POST[newpassword]", "$userxml-password");
    if ($datask === true) { $notice = "You've changed your password!"; $logged = "no"; }
}


if (isset($_POST['login'])){   
    $currentpage = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $datask = $Postings->Login("$_POST[username]", "$_POST[password]", "$currentpage");
    if ($datask === true) { $notice = "You've been logged in."; }
    if ($datask === false) { $notice = "Incorrect username and password."; }
}
if (isset($_POST['updatepage']) && $logged == "yes" && $userxml->username != 'demo'){
    $datask = $Postings->UpdatePage($_POST, $staceyversion);
    if ($datask > 0) { $notice = "You've saved the page!"; }
    if ($datask === false) { $notice = "The page failed to save!"; }
    // print_r($datask);
}
    
if (isset($_POST['uploadAttachment']) && $logged == "yes" && $userxml->username != 'demo'){
    $datask = $Postings->UploadAttachment($_FILES, $_POST['pagepath'], $_POST['pagefolderpath']);
    if ($datask === true) { $notice = "You've uploaded an attatchment!"; }
    if ($datask === false) { $notice = "Error uploading file"; }
}

if (isset($_POST['addsubpage']) && $logged == "yes" && $userxml->username != 'demo'){
    if (!isset($_POST['attachment'])){ $attachmentsposted = null; }
        else { $attachmentsposted = $_POST['attachment']; }
    $datask = $Postings->CreatePage("$_POST[pagename]", "$attachmentsposted", "$_POST[template]", "$_POST[pagepath]");
    if ($datask === true) { $notice = "You've created a sub-page!"; }
    
    //$pagename = $_POST[pagename]; $attachment = $_POST[attachment]; $template = $_POST[template]; $pagepath = $_POST[pagepath];
    
}


/*  Define variables
_______________________*/
$caseyversion = "1.6";

//exit($logged);

if (isset($logged)) { $loggedin = "yes"; } else { $loggedin = "no"; }
$mode = null; $name = null;
if (isset($_GET['mode'])){ $mode = $_GET['mode']; }
// exit($mode);
    elseif (isset($_REQUEST['mode'])) { $mode = $_REQUEST['mode']; }
if (isset($_GET['name'])){ $name = $_GET['name'];  }
if (isset($_GET['notice'])){ $notice = $_GET['notice'];  }
if ($name == "home") { $webname = ""; } 
    else { 
        $webname = $name; 
        //exit("webname is <b>$webname</b>");
        $webname = preg_replace("/([0-9].)([a-zA-Z]*)/i", "$2", $webname);
        // exit("webname is <b>$webname</b>");
        $webname = $webname."/";
    }

if (isset($notice)){ $shownotice = "yes"; } else { $shownotice = "no"; }
$thispage = explode("/", $name); $thispage = array_pop($thispage);
if (isset($userxml)){ $myusername = $userxml->username; }
if ($name == "home"){ $directory = "../content/index/"; }
    else { $directory = "../content/$name/"; }
    //echo "staceyversion is $staceyversion";

    if ($staceyversion == "2.3"){
        $files = glob("" . $directory . "*.txt");
    } elseif($staceyversion == "3"){
        $files = glob("" . $directory . "*.yml");
    }
    //print_r($files); exit();
    if (count($files) > 0){
        $templatefileexists = true;
        $pagepath = $files[0]; 
        $parentpath = dirname($directory); 
        $parentpath = str_replace("../content/", "", $parentpath);
        $currentpagepath = str_replace("../", "", $pagepath);
        $currentpagepath = explode("/", $currentpagepath); array_pop($currentpagepath); $currentpagepath = implode("/", $currentpagepath);
    if (empty($currentpagepath)) { $currentpagepath = "content/"; }
    $pagefolderpath = "../".$currentpagepath;
    $paragraphpagefolderpath = str_replace("../", "", $pagefolderpath);
    // print_r($currentpagepath);
    if ($parentpath == "../content"){ $parentpage = "index.php"; }
        else { $parentpage = "index.php?mode=page&name=$parentpath";  }
    } else {
        $templatefileexists = false; 
        $parentpath = dirname($directory);
        $pagepath = null;
        $currentpagepath = str_replace("../content/", "", $directory);
        $parentpage = "index.php?mode=page&name=$parentpath";
        if ($parentpath == "../content"){ $parentpage = "index.php"; }
        $pagefolderpath = "../content/".$currentpagepath;
        $paragraphpagefolderpath = str_replace("../", "", $pagefolderpath);
        // exit($currentpagepath);
    }


/*  Show: Page Listing
_______________________*/
if (isset($mode) && empty($_POST) || empty($mode)) {
$allpages = array();
    foreach(glob('../content/*', GLOB_ONLYDIR) as $dir) {
        $foldername = explode("/", $dir);
        if ($foldername[2] == "index") { $allpages[] = "home";  }
        elseif ($foldername[2] == "json") { }
        else { $allpages[] = $foldername[2]; 
        }
    }
}
if (isset($mode) && $mode == "logout"){
    $currentpage = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $adminpage = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $adminpage = explode("index.php", $adminpage);
    $adminpage = $adminpage[0];
    setcookie("pass", 'loggedout',time()+(60*360), "/", "");
    setcookie("publickey", 'loggedout',time()+(60*360), "/", "");
    header("Location: $adminpage");
}
         
if (isset($mode) && $mode == "page"){
    
    /*  Show: Edit Content
    _______________________*/          
    //$template//$templatename <--|
    if ($templatefileexists === false){ $files = null; }
    elseif ($staceyversion == "2.3"){ $files = glob("" . $directory . "*.txt"); }
    elseif ($staceyversion == "3"){ $files = glob("" . $directory . "*.yml"); }
    if ($templatefileexists === true){
        $readthis = file_get_contents($files[0]);
        $template = explode("/", $files[0]); $template = array_pop($template);
        $templatename = explode(".", $template); $templatename = $templatename[0];
        $lookfor = $directory.$template;
    }
    
    //------>check for writing permissions
    if (!empty($_GET['name']) && !is_writable("$directory")){ 
        echo "content/$_GET[name] is not writable. <br/>Please chmod it to 777 before you edit any content with Casey.";  exit(); 
    }
    if (!empty($_GET['name']) && !is_writable("$directory")){ 
        echo "content/$_GET[name] is not writable. <br/>Please chmod it to 777 before you edit any content with Casey.";  exit(); 
    }

    //------>parse the file to grab the data from it
    if ( isset($lookfor) && 0 == filesize( $lookfor ) && $templatefileexists === true){
        $templatepath = "../templates/".$template; $templatepath = explode(".", $templatepath);  $templatepath = array_slice($templatepath, 0, -1); $templatepath = implode($templatepath); $templatepath = "..".$templatepath.".html";
        if ($_GET['name'] != "feed") { $checkthis = file_get_contents($templatepath); }
        //preg_match_all("/(?<!\w)@\w*\b/", $checkthis, $matches); //keep the @ from the matches 
        preg_match_all("/(?<=@)\w*\b/", $checkthis, $matches);  //drop the @ from the matches (words beginning with @)
        $matches = $matches[0]; $matches = array_unique($matches);
        foreach ($matches as $attribute => $value){
            if ($value != "root_path" ) { $writethis .= "$value".": ________\n-\n"; }
        }
        if ($staceyversion == "2.3"){
            $writethis = explode("-", $writethis); $writethis = array_slice($writethis, 0, -1); $writethis = implode("-", $writethis);
            
            //echo "<textarea class=\"mediumtext\" name=\"$broken[0]\" style=\"width: 400px; height: 300px;\">"; echo($writethis); echo "</textarea><hr/>";
            $realcontent = strpos("$lookfor", "../content/");
            if ($realcontent !== false) {
                $fh = fopen($lookfor, 'w') or die ("could not open file");
                if (!empty($writethis)) { fwrite($fh, $writethis) or die ("could not save file"); fclose($fh); } else { $blanktxtfile = "yes"; }
                //don't save the file upon launching the edit page, unless it isn't empty
            }
            //echo $blanktxtfile; exit();
            $readthis = $writethis;
        } elseif ($staceyversion == "3"){
            // $writethis = explode("-", $writethis); $writethis = array_slice($writethis, 0, -1); $writethis = implode("-", $writethis);
            
            //echo "<textarea class=\"mediumtext\" name=\"$broken[0]\" style=\"width: 400px; height: 300px;\">"; echo($writethis); echo "</textarea><hr/>";
            $realcontent = strpos("$lookfor", "../content/");
            if ($real !== false) {
                $fh = fopen($lookfor, 'w') or die ("could not open file");
                if (!empty($writethis)) { fwrite($fh, $writethis) or die ("could not save file"); fclose($fh); } else { $blanktxtfile = "yes"; }
                //don't save the file upon launching the edit page, unless it isn't empty
            }
            //echo $blanktxtfile; exit();
            $readthis = $writethis;
        }
            
    }
    if (!isset($blanktxtfile)){ $blanktxtfile = "no"; }
    // if ($templatefileexists === false) { $missingtxtfile = ""}

        

    if ($templatefileexists === true) { 
        $fields = preg_split("/^[-]{1,3}(?![-])(?=\W*)/m", $readthis); 
        $matchcount = count($fields);
        //echo ("There are $matchcount matches<hr>"); 
        //print_r($fields); exit();
        
        
    }   
        /* what does that regex query do?
        "/m" means "multiline modifier.
        The "g" global modifier is included by default with
        preg_match_all
        PART 1 ---> ^[-]{1,3}
        Match the - character but only if it appears 1-3 times
        PART 2 ---> (?![-])
        "Negative lookahead" for if a "-" character is found
        afterwards in the match (disregard the match)
        // https://www.regular-expressions.info/lookaround.html
        ______
        PART 3 ---> (?=\W*)
        /The better version of "$". It detects the end of the
        line with support for various weird line endings
        (windows/mac/linux)
        // https://stackoverflow.com/a/8697057/337306
        ______
        PART 4 ---> $
        end of line "control character"
        //$fields = explode("\n-", $readthis); //v1.4
        */
        //print_r($fields); exit();
        // test regex at rubular.com and regex101.com
    
    //------>get the parsed data and populate an array with the appropiate information
    $counter = 0; $editableContent = array();
    
    if ($staceyversion == "2.3"){
        
        if (!empty($fields)) { foreach ($fields as $field){
            //echo "<textarea style=\"width: 300px; height: 200px;\">"; print_r($field); echo "</textarea>"; echo "<hr>";
            $broken = explode(": ", $field);
            if (strpos($broken[0], ":\n")){ $broken = explode(":\n", $broken[0]); } 
                // the above line is for in case .txt says ":\nVALUE" instead of "var: VALUE";
                // var_dump($broken); echo "<hr>";
                //print_r($kroken); echo "<hr>";
                //echo "<textarea style=\"width: 300px; height: 200px;\">"; print_r($broken); echo "</textarea>"; 
                //echo "<textarea style=\"width: 300px; height: 200px;\">"; print_r($kroken); echo "</textarea>"; 
                //echo "<hr>";
            
                $attribute = $broken[0];
                $value = implode(": ", $broken); 
                $value = htmlentities(str_replace("$broken[0]".": ", "", $value));
                if (strlen($value) >64){ $long = "yes"; } else { $long = "no"; }
                if (strlen($value) < 200){ $tinylong = "yes"; } else { $tinylong = "no"; }
                
                if (!empty($value)){
                    $editableContent[$counter]['attribute'] = $attribute;
                    $editableContent[$counter]['value'] = $value;
                    $editableContent[$counter]['long'] = $long;
                    $editableContent[$counter]['tinylong'] = $tinylong;
                }
            
              $counter ++;
        } }
        
    }
    if ($staceyversion == "3"){
        
        if (!empty($fields)) { foreach ($fields as $field){
            //echo "<textarea style=\"width: 300px; height: 200px;\">"; print_r($field); echo "</textarea>"; echo "<hr>";
            $broken = explode(": ", $field);
            // echo "$pagepath<hr>"; 
            $yamldata = spyc_load_file("$pagepath");
            // print_r($yamldata);
            // exit();
            // if (strpos($broken[0], ":\n")){ $broken = explode(":\n", $broken[0]); } 
            // the above line is for in case .txt says ":\nVALUE" instead of "var: VALUE";
            //print_r($broken); echo "<hr>";
            //print_r($kroken); echo "<hr>";
            //echo "<textarea style=\"width: 300px; height: 200px;\">"; print_r($broken); echo "</textarea>"; 
            //echo "<textarea style=\"width: 300px; height: 200px;\">"; print_r($kroken); echo "</textarea>"; 
            //echo "<hr>";
            foreach ($yamldata as $key => $value){
                $attribute = $broken[0];
                // $value = implode(": ", $broken); 
                // $value = htmlentities(str_replace("$broken[0]".": ", "", $value));
                if (strlen($value) >64){ $long = "yes"; } else { $long = "no"; }
                if (strlen($value) < 200){ $tinylong = "yes"; } else { $tinylong = "no"; }
                
                
                if (!empty($value)){
                    $editableContent[$counter]['attribute'] = $key;
                    $editableContent[$counter]['value'] = $value;
                    $editableContent[$counter]['long'] = $long;
                    $editableContent[$counter]['tinylong'] = $tinylong;
                }
            
                $counter ++;
                }
            }
            
        }   
        
    }
    
    // echo "<hr>"; print_r($editableContent);

    /*  Show: New Sub-page
    _______________________*/
    
    $files = glob("" . $directory . "*");
     if (count($files) >= 1){
        $counter = 0;
        $pagesAvailable = array();
        foreach ($files as $file){
        $pagename = str_replace ("$directory", "", $file); 
        if (!strpos($file, ".txt") && !strpos($file, ".yml") && !is_dir($file) ) {
            $pagesAvailable [$counter]['file'] = $file;
            $pagesAvailable [$counter]['pagename'] = $pagename;
        }
        $counter ++;
        }
        $ifAttachments = array();
        if (!empty($pagesAvailable)){ $ifAttachments[0] = 'yes'; }
    }
        
        $templatesFolder = "../templates/";
        $files = glob("" . $templatesFolder . "*.html");
        $templatesAvailable = array();
        foreach ($files as $file){
        $file = str_replace ("../templates/", "", $file);
        $templatesAvailable[] = $file;
        }
    
    /*  Show: Attachments
    _______________________*/
    $files = glob("" . $directory . "*");
    $counter = 0; $attachments = array();
    foreach ($files as $file){
        if (!strpos($file, ".yml") && !is_dir($file)){
            $pagename = str_replace ("$directory", "", $file); 
            // print_r($pagename);
            //echo "<br>".$pagename."<br>";
            $filetype = explode(".", $pagename); 
                        //array_pop($filetype); 
            // echo "<u>$filetype[1]</u>";
            //$filetype = $filetype['1']; 
            $filetype = end($filetype);
            // get last element of array
            // print_r($filetype);

        } else { $filetype = NULL; }
        // exit();
    // echo "<br>".$filetype[0];/
        if ($filetype == "png" || $filetype == "gif" || $filetype == "jpg") { $icon = "photo"; $type="image"; }
        elseif ($filetype == "mp4" || $filetype == "mov" || $filetype == "flv") { $icon = "film"; $type="video"; }
        elseif ($filetype == "mp3" || $filetype == "ogg" || $filetype == "aac") { $icon = "film"; $type="audio"; }
        elseif ($filetype == "swf") { $icon = "page_white_flash"; $type="flash"; }
        elseif ($filetype == "pdf"){ $icon = "page_white_acrobat"; $type="pdf"; }
        else { $icon ="page"; $type="document"; }
        if (!strpos($file, ".txt") && !strpos($file, ".yml") && !is_dir($file) ) { 
            $attachments [$counter]['icon'] = $icon;
            $attachments [$counter]['pagename'] = $pagename;
            $attachments [$counter]['type'] = $type;
        }
        $counter ++;
        }
    } //close if not posted yet
    // print_r($attachments);
    
    /*  Show: Sub-pages
    _______________________*/
    $files = glob("" . $directory . "*");
    $counter = 0; $subpages = array();
    foreach ($files as $file){
        //echo $file;
        $linkpath = str_replace ("../content/", "", $file); 
        $pagename = str_replace ("$directory", "", $file); 
        $filetype = explode(".", $file); $filetype = $filetype[1];
        if (is_dir($file)) {
            $subpages[$counter]['file'] = $pagename;
            $subpages[$counter]['linkpath'] = $linkpath;
            $counter ++;
        }
    }
//} //close if $_GET[page] 
if (isset($mode) && $mode == "account"){
    $users = $xml;
    $users = xml2array($users);
    $users = $users['user'];
}


/*  Load templating engine
    _______________________*/
$TBS->LoadTemplate('theme_two.html');
if (empty($mode)) {
    $TBS->MergeBlock('allpages',$allpages);
}elseif (isset($mode) && $mode == "page"){
    $TBS->MergeBlock('subpage',$subpages);
    $TBS->MergeBlock('templates',$templatesAvailable);
    $TBS->MergeBlock('pagesAvailable',$pagesAvailable);
    $TBS->MergeBlock('ifAttachments',$ifAttachments);
    $TBS->MergeBlock('attachments',$attachments);
    $TBS->MergeBlock('content',$editableContent);
}elseif (isset($mode) && $mode == "account"){
    $TBS->MergeBlock('users',$users);
}

if (isset($mode) && $mode != "getversion" || empty($mode)){ $TBS->Show(); }
?>
