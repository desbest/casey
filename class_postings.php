<?php
require("users.php");
require_once "spyc.php";
// print_r($_POST); exit();
class Postings {
    /* Contents
        ----> DoIKnowMyPassword
        ----> UpdatePage            
        ----> UploadAttatchment     
        ----> CreatePage
        ----> Login
        ----> ChangePassword
        ----> Slugify
    */

    function DoIKnowMyPassword($username, $password){
        require ("class_arraytoxml3.php");
        $ATX3 = new ArrayToXML3;
    
        // $password = md5($password);
        //$allusers = file_get_contents("users.php"); 
        //$allusers = explode("//_", "$allusers");  $allusers = array_slice($allusers, 2, -1); $allusers = implode($allusers);
        $allusers = include("users.php");   
        
        $xml = new SimpleXMLElement($allusers);
        //echo "<textarea style=\"width: 400px; height: 300px;\">";print_r($xml); echo "</textarea>";
        
        // $nodes = $xml->xpath(sprintf(" //users/user[(username = \"$username\") and (password = \"$password\")] ")); 
        $nodes = $xml->xpath(sprintf(" //users/user[(username = \"$username\")] ")); 
        if (!empty($nodes)) {
            $userxmlRaw = $ATX3->toXml($nodes[0]); $userxml = new SimpleXMLElement($userxmlRaw);
            //echo "<textarea style=\"width: 400px; height: 300px;\">\n";print_r($userxml); echo "</textarea><hr />";
            //echo "<textarea style=\"width: 400px; height: 300px;\">\n";print_r($userxmlRaw); echo "</textarea><hr />";
            $realpassenc = $userxml->password->__toString();
            $passwordcheck = password_verify("$password", $realpassenc);
            // print_r($realpassenc); echo "<hr>$password<hr>"; print_r($passwordcheck); echo "<hr>";    
            if ($passwordcheck === true){ 
                return true;
            } else {
                return false;
            }
        }
    }

    function Slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // trim
        $text = trim($text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
        // lowercase
        $text = strtolower($text);

        if (empty($text)) { return 'n-a'; }
        return $text;
    }

    
    function ChangePassword($username, $password1, $oldpassword, $password2){

        // bycrypt password encryption
        $allusers = include("users.php");    
        // print_r($allusers);
        $sxml = new SimpleXMLElement($allusers);
        // print_r($sxml);
        // exit();

        $username = "desbest";
        $user = $sxml->xpath("./user[./username = '{$username}']")[0];
        $newpassword = password_hash($newpassword, PASSWORD_DEFAULT);
        $user->password = $newpassword;

        $newusersxml = $sxml->asXML();
        $content = <<<'EOD'
       <?php
        if (preg_match("/users.php/", "$_SERVER[PHP_SELF]" )) {
            die("You are not allowed to access the users file.");
        }

        return '    
EOD;
        $content .= $newusersxml;
        $content .= <<<EOD
        ';
        ?>
EOD;
        $content = str_replace('<?xml version="1.0"?>', "", $content);
        $task = file_put_contents("users.php", $content);
        

        if ($task === true){ 
            //setcookie("pass", $newpassword,time()+(60*360), "/", "");
            //setcookie("publickey", $publickey,time()+(60*360), "/", "");
            header("Refresh:0");
            return true; 
        } else { return false; }

        // md5 password encryption
        /* 
        $oldPasswordHash = md5($newpassword);
        $newPasswordHash = md5($oldpassword);
        $myFile = "users.php";
        $contents = file_get_contents($myFile);
        $savethis = str_replace("$oldPasswordHash", "$newPasswordHash", $contents);
        $fh = fopen($myFile, 'w');
        if (!$fh) { return false; }
        fwrite($fh, $savethis); fclose($fh);             
        return true;
        */
    }
    
    function Login($username, $password, $currentpage){
    
        require ("class_arraytoxml2.php");
        $ATX2 = new ArrayToXML2;
    
        // $password = md5($password);
        //$allusers = file_get_contents("users.php"); 
        //$allusers = explode("//_", "$allusers");  $allusers = array_slice($allusers, 2, -1); $allusers = implode($allusers);
        $allusers = include("users.php");   
        
        $xml = new SimpleXMLElement($allusers);
        //echo "<textarea style=\"width: 400px; height: 300px;\">";print_r($xml); echo "</textarea>";
        
        // $nodes = $xml->xpath(sprintf(" //users/user[(username = \"$username\") and (password = \"$password\")] ")); 
        $nodes = $xml->xpath(sprintf(" //users/user[(username = \"$username\")] ")); 
        if (!empty($nodes)) {
            $userxmlRaw = $ATX2->toXml($nodes[0]); $userxml = new SimpleXMLElement($userxmlRaw);
            //echo "<textarea style=\"width: 400px; height: 300px;\">\n";print_r($userxml); echo "</textarea><hr />";
            //echo "<textarea style=\"width: 400px; height: 300px;\">\n";print_r($userxmlRaw); echo "</textarea><hr />";
            $realpassenc = $userxml->password->__toString();
            $passwordcheck = password_verify("$password", $realpassenc);
            // print_r($realpassenc); echo "<hr>$password<hr>"; print_r($passwordcheck); echo "<hr>";    
            if ($passwordcheck === true){
            // if (hash_equals($realpassenc, $passwordcheck)){
                $publickey = $userxml->publickey;
                setcookie("pass", $realpassenc,time()+(60*360), "/", "");
                setcookie("publickey", $publickey,time()+(60*360), "/", "");
                // echo "<meta http-equiv=\"refresh\" content=\"0\">";
                header("Location: $currentpage");
                return true; //rendered useless by redirect
            }
        } else {
            return false;
        }    
    }
    
    function CreatePage($pagename, $attachment, $template, $pagepath){
        
        if (empty($pagename)){ //do attachment
            $callme = explode(".", "$attachment"); $callme = array_slice($callme, 0, -1); $callme = implode($callme); $callme = "..".$callme; $callme = ucwords($callme);
        }
        if (empty($attachment)){ //do page name
            $callme = $pagepath.$pagename;
        }
        
        $callme = strtolower($callme);
        $callme = str_replace(" ", "-", $callme);
        //$callme = ucfirst($callme);
        $datask = mkdir("$callme", 0777); 
        $usetemplate = explode(".", $template); $usetemplate = $usetemplate[0];
        $makewhere = $callme."/$usetemplate".".txt";
        $ourFileHandle = fopen($makewhere, 'w') or die("can't open file");
        fclose($ourFileHandle);        
        if (!$ourFileHandle){ return false; }
        return true;    
    }
    
    function UploadAttachment($filechosen, $pagepath, $pagefolderpath){
        $filename = basename( $filechosen['uploadedfile']['name']); 

        if (!empty($pagepath)){
            $uploadto = explode("/", $pagepath); $uploadto = array_slice($uploadto, 0, -1); $uploadto = implode("/", $uploadto);     
        } else { $uploadto = $pagefolderpath; }
        
        $uploadas = $uploadto."/".$filename;
        if(move_uploaded_file($filechosen['uploadedfile']['tmp_name'], $uploadas)) {
            return true;
        } else{ return false; }
    }
    
    function UpdatePage($postInput, $staceyversion){                      
        $posted = $postInput; $posted = array_slice($posted, 0, -1);
        $savethis = ""; // php 7 fix to initialise a previously unknown variable
        if ($staceyversion == "2.3"){
            foreach ($posted as $attribute => $value){
                $attribute=preg_replace('`[\r\n]+`',"\n",$attribute);
                $attribute=str_replace("<br_/>", "", $attribute);
                $value = stripslashes(htmlspecialchars_decode($value));
                $savethis .= "$attribute".": "."$value"."\n-"."";
            }
            if ($_GET['name'] == "home"){ $directory = "../content/index/"; }
                else { $directory = "../content/$_GET[name]/"; }
            
            $files = glob("" . $directory . "*.txt");
            $myFile = $files[0];
            $findValidContent = strpos($myFile, "../content/");

            if ($findValidContent !== false) { 
                $fh = fopen($myFile, 'w') or die("could not open file");
                if (!$fh) { return false; echo "could not open file"; exit(); }
                $task = fwrite($fh, $savethis) or die("could not save page!"); fclose($fh);      
            }    
        }
        if ($staceyversion == "3"){
            $posted = $postInput; $posted = array_slice($posted, 0, -1);
            // foreach ($posted as $attribute => $value){
            //     $attribute=preg_replace('`[\r\n]+`',"\n",$attribute);
            //     $attribute=str_replace("<br_/>", "", $attribute);
            //     $value = stripslashes(htmlspecialchars_decode($value));
            //     $savethis .= "$attribute".": "."$value"."\n-"."";
            // }
            if ($_GET['name'] == "home"){ $directory = "../content/index/"; }
                else { $directory = "../content/$_GET[name]/"; }

            $files = glob("" . $directory . "*.yml");
            $myFile = $files[0];
            $findValidContent = strpos($myFile, "../content/");

            $savethis = spyc_dump($posted); 
            // print_r($findValidContent); exit();


            if ($findValidContent !== false) { 
                $fh = fopen($myFile, 'w') or die("could not open file");
                if (!$fh) { return false; echo "could not open file"; exit(); }
                $task = fwrite($fh, $savethis) or die("could not save page!"); fclose($fh); 
                // print_r($task); 
            }
             // exit();
        }
        
        if ($task === true || $task > 0) { return true; } else { return false; }

        }

    
}    
?>