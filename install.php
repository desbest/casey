<?php $_REQUEST['mode'] = "getversion"; include("index.php"); ?>
<html>
<head>
<title>Casey Installer</title>
<meta name="viewport" content="width=400">
<link rel="stylesheet" type="text/css" href="resources/stylesheet_install.css" />
</head>
<body>

<div id="navcontainer">
<ul id="navlist">
<?php 
if (empty($_GET['step'])) { echo "<li id=\"active\"><a id=\"current\">Step 1</a></li>"; } else { echo "<li><a>Step 1</a><li>"; }
if (isset($_GET['step']) && $_GET['step'] == 2) { echo "<li id=\"active\"><a id=\"current\">Step 2</a></li>"; } else { echo "<li><a>Step 2</a><li>"; }
?>
</ul>
</div>

<div class="offset">
<font class="header">Install Casey</font>
<br>
        
    <?php

    //find out which version of stacey cms the person is using
    $directory = "../content/";
    $prevvercount = count(glob("" . $directory . "*.txt"));
    $nextvercount = count(glob("" . $directory . "*.yml"));
    if ($prevvercount > 0){ $staceyversion = "2.3"; }
    if ($nextvercount > 0){ $staceyversion = "3"; }
    //////

    if (empty($_GET['step']) || $_GET['step'] == 1){
    echo "
        Casey is licensed under the MIT license.
<br>
<textarea class=\"mediumtext\" style=\"width: 500px; height: 200px;\">
Copyright (C) 2011-2020 by desbest

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the \"Software\"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
</textarea>

<br/>
<br/>Casey $caseyversion
<!-- That variable is defined in Line 75 of index.php -->
<br/>First created: 05/08/2011
<br/>Last updated: Check the gitlab repository
<br/>by <a href=\"http://desbest.com\">desbest</a>
    
    <hr/>
    <a id=\"permissions\"></a>
    <font class=\"subheader\">PERMISSIONS</font>
    <br>To install Casey, you need to chmod the following files and folders.
    <br/>This might already be done by your server's settings.
    <br/>
    <br/>cache/ (755) <br/>content/ (755) <br/>users.php (644)
    <br/>
    ";
    if (is_writable("../app/_cache") && is_writable("../content") && is_writable("users.php") ){ 
        echo "<br/><a href=\"install.php?step=2\">You can continue now.</a>";
    }
    else { echo "<br/><a href=\"install.php\">Check again.</a>"; }
    
    }
    
    $userfile = file_get_contents("users.php");
    if (isset($_GET['step']) && $_GET['step'] == 2 && empty($_POST)){
        if ((strlen($userfile)) > 233){
        echo "
            You cannot continue installing Casey, because a user account is already configured.
            <br/>If you want to add a user account, edit users.php manually to do so.
        ";
        } else {
        echo "

            This installer only lets you create 1 account.
            <br/>For that reason, if you're setting Casey up for a client, let them complete part 2 of this installer.
            <br/>If you make a mistake in this form, the user file will have to be wiped blank.
            <form method=\"POST\">
            <br/><strong>Setup your account</strong>
            <br/>Username: <input type=\"text\" class=\"bigtext\" name=\"username\" />
            <br/>Email: <input type=\"text\" class=\"bigtext\" name=\"email\"/>
            <br/>Password: <input type=\"password\" class=\"bigtext\" name=\"password1\" />
            <br/>Confirm password: <input type=\"password\" class=\"bigtext\" name=\"password2\" />
            <br/>
            <br/><input type=\"submit\" name=\"createuser\" class=\"bigbutton\" value=\"Create account\" />
            </form>
            </div>
        ";
        }
    } 
    
    function getUniqueCode($length = "")
    {   
    $code = md5(uniqid(rand(), true));
    if ($length != "") return substr($code, 0, $length);
    else return $code;
    }

    // echo (strlen($userfile)); exit();
    // if ((strlen($userfile)) > 232) { exit("ghghgh"); }
    
    if (isset($_POST['createuser']) && ((strlen($userfile)) > 232) ){
       $random = getUniqueCode(); 
        // $password = md5($_POST[password]);
       $password1 = $_POST['password1'];
       $password2 = $_POST['password2'];
       if($password1 != $password2) { echo "The passwords do not match.<hr>"; exit("xx"); }
        $hashedpassword = password_hash($password1, PASSWORD_DEFAULT);

        $savethis = null;
    
        $savethis .= '<?php
        $staceyversion = "'.$staceyversion.'"; //"2.3 or 3"

        if (preg_match("/users.php/", "$_SERVER[PHP_SELF]" )) {
            die("You are not allowed to access the users file.");
        }'; $savethis.= "

        return '
        
        <users>
            <![CDATA[
            <!-- To create a new user
            <!-- bcrypt the password at https://bcrypt-generator.com/ -->
            <!-- md5 the publickey at https://quickhash.com/ and store it lower case -->
            ]]>
            
            <user>
            <username>$_POST[username]</username>
            <email>$_POST[email]</email>
            <password>$hashedpassword</password>
            <publickey>$random</publickey>
            </user>

            <user>
            <username>demo</username>
            <email>nobody@example.com</email>
            <password>$2y$10$.GhEMiWg/j/R08YaxcuPEOxrL3ozN5TJXejH2dFQv.RxYKdMcnfIW</password>
            <publickey>fe01ce2a7fbac8fafaed7c982a04e229</publickey>
            </user>
            

        </users>
        
        ';?>";
     
        $myFile = "users.php";
        $fh = fopen($myFile, 'w');
        if (!$fh) { return false; }
        $datask = fwrite($fh, $savethis) or die ("the user file failed to write!"); fclose($fh);             
        
        echo "
            <img src=\"resources/bigtick.png\" align=\"left\"/>
            <br/>Casey is now installed. Well nearly, edit the users.php file manually to show whether Stacey 2.3 or 3 is installed.
            <br/>You may want to rename /casey to /admin
            <br/>The installer no longer functions, so you can leave it online.
            <br/>
            <br/><a href=\"index.php\">Now you can login.</a>
        ";
        //echo "<textarea class=\"mediumtext\" style=\"width: 500px; height: 200px;\">$writethis</textarea>";
    }
    ?>

</div>
</body>
</html>