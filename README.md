# Casey

Casey is a frontend for the flat file cms called [Stacey](http://github.com/kolber/stacey/) so you can edit your website in a web browser instead of resorting to FTP or WebDAV. Information about Stacey 2.3 can be [found here](http://github.com/kolber/stacey-site) and for Stacey 3 in [the wiki](https://github.com/kolber/stacey/wiki).

It supports both Stacey 2.3 and Stacey 3, of which version 3 isn't backwards compatible.

[More information and demo is here](http://desbest.com/casey) or on the [gitlab wiki](https://gitlab.com/desbest/casey/-/wikis/home). You can see [the changelog here](https://gitlab.com/desbest/casey/-/wikis/Changelog).

## Copyright
Copyright desbest 2011-2021

MIT LIcense

## More information

The client wants: A website where different pages have different layouts. This means that after designing the header and footer, that the position the content goes in, will vary by page. 

The problem: Traditional cms's weren't designed for this purpose. Most cms scripts like Wordpress work in a header-content-footer fashion, where the content has a fixed layout. 

The solution: Casey. Casey supports attachments and menus; and unlike a point-and-click cms, you can make new pages

For information on how to use Stacey, see more if you use [version 2.3](http://desbest.com/stacey2.3) or [version 3](http://github.com/kolber/stacey/wiki)
